package com.studyroom.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.studyroom.pojo.FoodAddress;
import com.studyroom.pojo.ViewLevel;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author wuhongbin
 * @version 1.0.0
 * @ClassName FoodAddressMapper.java
 * @Description TODO
 * @createTime 2022年03月13日 14:58:00
 */
@Repository
public interface ViewLevelMapper extends BaseMapper<ViewLevel> {

    @Select("SELECT * FROM t_view_level WHERE is_delete = 0")
    List<ViewLevel> getViewLevel();


    @Select("SELECT * FROM t_view_level WHERE is_delete = 0 ORDER BY id ASC")
    List<FoodAddress> getViewLevelOrder();
}
