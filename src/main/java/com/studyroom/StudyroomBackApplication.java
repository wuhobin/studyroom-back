package com.studyroom;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.redis.RedisAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jooq.JooqAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

@MapperScan("com.studyroom.mapper")
@SpringBootApplication
public class StudyroomBackApplication {

    public static void main(String[] args) {
        SpringApplication.run(StudyroomBackApplication.class, args);
    }

}
