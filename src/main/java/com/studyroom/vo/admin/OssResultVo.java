package com.studyroom.vo.admin;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class OssResultVo {
    private Integer officeFileId;
    private Integer imageId;
    private String imageName;
    private String imageUrl;
}
