package com.studyroom.service.oss;

import com.studyroom.vo.admin.OssResultVo;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author wuhongbin
 * @version 1.0.0
 * @ClassName OssServoce.java
 * @Description TODO
 * @createTime 2022年03月13日 16:07:00
 */
public interface OssService {

    /**
     * 上传文件  图片
     * @param imageFile 图片文件
     * @return 结果
     */
    OssResultVo uploadImage(MultipartFile imageFile);
}
