package com.studyroom.service.food;

import com.studyroom.util.ResponseEntity;
import com.studyroom.vo.admin.FoodParamVo;

/**
 * @author wuhongbin
 * @createTime 2022年03月13日 14:05:00
 */
public interface FoodService {

    /**
     * 查询美食列表
     * @param pageNum 页码
     * @return null
     */
    ResponseEntity<?> getFoodList(Integer pageNum);


    /**
     * 获取所有食物地名列表
     * @return "
     */
    ResponseEntity<?> getAddressNameList();


    /**
     * 根据id删除
     * @param id  id
     * @return d
     */
    ResponseEntity<?> deleteFood(Integer id);

    /**
     * 添加礼物
     * @param paramVo:
     * @return null
     */
    ResponseEntity<?> addFood(FoodParamVo paramVo);

    /**
     * 修改食品
     * @param paramVo:
     * @return null
     */
    ResponseEntity<?> updateFood(FoodParamVo paramVo);
}
