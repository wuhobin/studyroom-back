package com.studyroom.constant;

/**
 * Session常量
 * @author 刘博
 */
public class SessionConstant {

    /**
     * app session
     */
    public static final String SESSION_APP_USER = "zx_invest_pension_app";
    /**
     * WeChat session
     */
    public static final String SESSION_WE_CHAT_USER = "zx_invest_pension_weChat";
    /**
     * 后台admin session
     */
    public static final String SESSION_ADMIN_USER = "zx_invest_pension_admin";
}
