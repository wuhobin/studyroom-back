package com.studyroom.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.studyroom.pojo.Food;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author wuhongbin

 * @createTime 2022年03月13日 11:58:00
 */
@Repository
public interface FoodMapper extends BaseMapper<Food> {

    @Select(" SELECT id,name,description,image_id,image,create_time,update_time,is_delete,address_id FROM t_food WHERE is_delete = 0 ORDER BY id DESC LIMIT #{offset}, #{limit} ")
    List<Food> getFoodList(@Param("pageNum") Integer pageNum,@Param("offset") Integer offset, @Param("limit") Integer limit);

}
