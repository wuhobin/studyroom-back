package com.studyroom.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.studyroom.pojo.User;
import org.springframework.stereotype.Repository;

/**
 * @author wuhongbin
 */
@Repository
public interface UserMapper extends BaseMapper<User> {

}
