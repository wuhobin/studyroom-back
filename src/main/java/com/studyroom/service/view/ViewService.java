package com.studyroom.service.view;

import com.studyroom.util.ResponseEntity;
import com.studyroom.vo.admin.ViewParamVo;

/**
 * @author wuhongbin
 * @version 1.0.0
 * @ClassName ViewService.java
 * @Description TODO
 * @createTime 2022年03月16日 17:30:00
 */
public interface ViewService {
    /**
     * 获取景点列表
     * @param pageNum :L
     * @return :
     */
    ResponseEntity<?> getViewList(Integer pageNum);

    /**
     * 添加景点
     * @param paramVo :
     * @return :
     */
    ResponseEntity<?> addView(ViewParamVo paramVo);

    /**
     * 删除
     * @param id id
     * @return tr
     */
    ResponseEntity<?> deleteView(Integer id);

    /**
     * 更新
     * @param paramVo"
     * @return "
     */
    ResponseEntity<?> updateView(ViewParamVo paramVo);

    /**
     * 获取等级列表
     * @return :
     */
    ResponseEntity<?> getLevelNameList();
}
