package com.studyroom.controller.admin;

import com.studyroom.annotation.PassToken;
import com.studyroom.service.food.FoodService;
import com.studyroom.util.ResponseEntity;
import com.studyroom.vo.admin.FoodParamVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author wuhongbin
 * @createTime 2022年03月13日 14:04:00
 */
@RestController
@RequestMapping("/food")
public class FoodController {

    private final FoodService foodService;

    @Autowired
    public FoodController(FoodService foodService){
        this.foodService = foodService;
    }

    @PassToken
    @PostMapping("/list")
    public ResponseEntity<?> getFoodList(@RequestBody FoodParamVo foodParamVo){
        return foodService.getFoodList(foodParamVo.getPageNum());
    }

    @PostMapping("/add")
    public ResponseEntity<?> addFood(FoodParamVo paramVo){
        return foodService.addFood(paramVo);
    }

    @PassToken
    @GetMapping("/address-list")
    public ResponseEntity<?> getAddressNameList(){
        return foodService.getAddressNameList();
    }

    @GetMapping("/delete/{id}")
    public ResponseEntity<?> deleteFood(@PathVariable("id") Integer id){
        return foodService.deleteFood(id);
    }


    @PostMapping("/update")
    public ResponseEntity<?> updateFood(FoodParamVo paramVo){
        return foodService.updateFood(paramVo);
    }


}
