package com.studyroom.controller.admin;

import com.studyroom.annotation.PassToken;
import com.studyroom.service.customs.CustomsService;
import com.studyroom.service.view.ViewService;
import com.studyroom.util.ResponseEntity;
import com.studyroom.vo.admin.CustomsParamVo;
import com.studyroom.vo.admin.ViewParamVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author wuhongbin
 * @createTime 2022年03月13日 14:04:00
 */
@RestController
@RequestMapping("/customs")
public class CustomsController {

    private final CustomsService customsService;

    @Autowired
    public CustomsController(CustomsService customsService){
        this.customsService = customsService;
    }

    @PassToken
    @PostMapping("/list")
    public ResponseEntity<?> getCustomsList(@RequestBody CustomsParamVo paramVo){
        return customsService.getCustomsList(paramVo.getPageNum());
    }

    @PostMapping("/add")
    public ResponseEntity<?> addCustoms(CustomsParamVo paramVo){
        return customsService.addCustoms(paramVo);
    }


    @GetMapping("/delete/{id}")
    public ResponseEntity<?> deleteCustoms(@PathVariable("id") Integer id){
        return customsService.deleteCustoms(id);
    }


    @PostMapping("/update")
    public ResponseEntity<?> updateCustoms(CustomsParamVo paramVo){
        return customsService.updateCustoms(paramVo);
    }


}
