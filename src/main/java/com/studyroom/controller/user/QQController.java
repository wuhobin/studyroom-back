//package com.studyroom.controller.user;
//
//import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
//import com.studyroom.annotation.PassToken;
//import com.studyroom.mapper.UserMapper;
//import com.studyroom.pojo.User;
//import com.studyroom.util.*;
//import io.lettuce.core.api.sync.RedisCommands;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Qualifier;
//import org.springframework.util.ObjectUtils;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.RestController;
//
//import java.io.IOException;
//import java.util.HashMap;
//import java.util.Map;
//
///**
// * @author wuhongbin
// * @version 1.0.0
// * @ClassName QQController.java
// * @createTime 2021年10月31日 15:57:00
// */
//@RestController
//@RequestMapping("/qq")
//public class QQController {
//
//    private final QqConstant qqConstant;
//    private final UserMapper userMapper;
//    private RedisCommands<String,String> redisCommands;
//
//    @Autowired
//    public QQController(QqConstant qqConstant, UserMapper userMapper, @Qualifier("syncCommand") RedisCommands<String,String> redisCommands) {
//        this.redisCommands = redisCommands;
//        this.qqConstant = qqConstant;
//        this.userMapper = userMapper;
//    }
//
//
//
//    /**
//     * 发起请求
//     */
//    @PassToken
//    @GetMapping("/login")
//    public ResponseEntity qq(){
//        return ResponseUtils.success(CommonResponseEnum.SUCCESS, qqConstant.qqLogin());
//    }
//
//    /**
//     * qq登录请求成功回调地址
//     * @param code
//     * @param state
//     * @return
//     */
//    @PassToken
//    @GetMapping("/callback")
//    public ResponseEntity qqCallback(@RequestParam("code") String code,
//                                     @RequestParam("state") String state) throws IOException {
//        Map<String, Object> map = new HashMap<>();
//        String accessToken = QQHttpClient.getAccessToken(qqConstant.getAccessToken(code));
//        String openID = QQHttpClient.getOpenID(qqConstant.getOpenID(accessToken));
//        QqModel userInfo = QQHttpClient.getUserInfo(qqConstant.getUserInfo(openID, accessToken));
//        map.put("accessToken",accessToken);
//        map.put("openID",openID);
//        String token = JwtTokenUtils.generateToken(map);
//        QueryWrapper<User> wrapper = new QueryWrapper<>();
//        wrapper.eq("oppen_id",openID);
//        User user = userMapper.selectOne(wrapper);
//        if (ObjectUtils.isEmpty(user)){
//            User mUser = new User();
//            mUser.setAccessToken(accessToken);
//            mUser.setNickname(userInfo.getNickname());
//            mUser.setUserAvatar(userInfo.getFigureurl_qq_1());
//            mUser.setGender(userInfo.getGender());
//            mUser.setOpenId(openID);
//            mUser.setLoginCount(1);
//            userMapper.insert(mUser);
//        }else{
//            user.setAccessToken(accessToken);
//            user.setNickname(userInfo.getNickname());
//            user.setUserAvatar(userInfo.getFigureurl_qq_1());
//            user.setGender(userInfo.getGender());
//            user.setOpenId(openID);
//            user.setLoginCount(user.getLoginCount() + 1);
//            userMapper.updateById(user);
//        }
//        return ResponseUtils.success(CommonResponseEnum.SUCCESS,new QqLoginVO(userMapper.selectOne(wrapper),token));
//    }
//}
