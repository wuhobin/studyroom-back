package com.studyroom.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * @author wuhongbin
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "t_customs")
public class Customs {
    @TableId(type = IdType.AUTO)
    private Integer id;
    private String name;
    @TableField("image_id")
    private Integer imageId;
    @TableField("image")
    private String image;
    private String description;
    private String time;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;
    private Integer isDelete;
}
