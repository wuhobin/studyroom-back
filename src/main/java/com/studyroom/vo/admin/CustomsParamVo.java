package com.studyroom.vo.admin;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author wuhongbin
 * @createTime 2022年03月13日 14:12:00
 */
@Data
public class CustomsParamVo {
    private Integer pageNum;

    private Integer id;

    private String  name;

    private String time;

    private String description;

    private MultipartFile image;
}
