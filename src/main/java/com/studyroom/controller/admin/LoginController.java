package com.studyroom.controller.admin;

import com.studyroom.annotation.PassToken;
import com.studyroom.mapper.UserMapper;
import com.studyroom.service.user.UserService;
import com.studyroom.util.ResponseEntity;
import com.studyroom.vo.admin.LoginParamVo;
import io.lettuce.core.api.sync.RedisCommands;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * @author wuhongbin
 * @createTime 2022年03月13日 12:03:00
 */
@RestController
@RequestMapping("/user")
public class LoginController {

    private final UserService userService;

    private final RedisCommands<String,String> redisCommands;

    public LoginController(UserService userService, @Qualifier("syncCommand") RedisCommands<String,String> redisCommands){
        this.redisCommands = redisCommands;
        this.userService = userService;
    }

    @PassToken
    @RequestMapping("/login")
    public ResponseEntity<?> login(@RequestBody @Valid LoginParamVo paramVo){
        return userService.loginByCaptcha(paramVo);
    }
}
