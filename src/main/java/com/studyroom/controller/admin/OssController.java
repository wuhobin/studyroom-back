package com.studyroom.controller.admin;

import com.studyroom.annotation.PassToken;
import com.studyroom.service.oss.OssService;
import com.studyroom.util.ResponseEntity;
import com.studyroom.util.ResponseUtils;
import com.studyroom.vo.admin.OssParamVo;
import com.studyroom.vo.admin.OssResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@RequestMapping("/api-oss")
public class OssController {
    private final OssService ossService;
    @Autowired
    public OssController(OssService ossService) {
        this.ossService = ossService;
    }

    @PassToken
    @PostMapping(value = "/upload-image")
    public ResponseEntity<?> uploadImage(OssParamVo operateVo) {
        MultipartFile imageFile = operateVo.getFile();
        OssResultVo ossResultVo = ossService.uploadImage(imageFile);
        if(ossResultVo==null){
            return ResponseUtils.body("400", "上传图片失败", true);
        }else{
            return ResponseUtils.body("200", "请求成功", ossResultVo);
        }
    }
}
