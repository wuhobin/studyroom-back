package com.studyroom.vo.admin;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author cwh
 */
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class PageInfoVo<T> {

    private List<T> data;
    private Integer count;
    private Integer pages;

}