package com.studyroom.vo;

import com.studyroom.pojo.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author wuhongbin
 * @version 1.0.0
 * @ClassName QqLoginVO.java
 * @Description qq登录成功返回的信息
 * @createTime 2021年10月31日 17:00:00
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class QqLoginVO {
    private User user;
    private String token;
}
