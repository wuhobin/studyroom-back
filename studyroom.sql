/*
 Navicat Premium Data Transfer

 Source Server         : 阿里云
 Source Server Type    : MySQL
 Source Server Version : 80028
 Source Host           : rm-bp1j8k9s2330jz5jdyo.mysql.rds.aliyuncs.com:3306
 Source Schema         : studyroom

 Target Server Type    : MySQL
 Target Server Version : 80028
 File Encoding         : 65001

 Date: 28/03/2024 17:06:30
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for t_customs
-- ----------------------------
DROP TABLE IF EXISTS `t_customs`;
CREATE TABLE `t_customs`  (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '节日名称',
  `description` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '节日描述',
  `image_id` int NULL DEFAULT NULL COMMENT '图片id',
  `image` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图片',
  `time` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '节日时间',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_delete` tinyint NOT NULL DEFAULT 0 COMMENT '是否删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_customs
-- ----------------------------
INSERT INTO `t_customs` VALUES (1, '1', '1', 1108500, 'image/studyroom/text_164873610658016c244e39517c1e577a0b778cb1ce568', NULL, '2022-04-01 06:15:06', '2022-04-01 06:15:06', 1);
INSERT INTO `t_customs` VALUES (2, '3', '3', 1108501, 'image/studyroom/text_1648736203712614594c34e0c9dc796cb21d5e806768b', 'Wed Mar 16 2022 22:16:38 GMT+0800', '2022-04-01 06:16:43', '2022-04-01 06:16:43', 1);
INSERT INTO `t_customs` VALUES (3, '女儿会', '被誉为 “土家情人节”，是与封建包办婚姻相对立的一种恋爱方式，是恩施土家族青年在追求自由婚姻的过程中，自发形成的以集体择偶为主要目的的节日盛会。其主要特征是以歌为媒，自主择偶。', 1108515, 'image/studyroom/text_1649487821534ebbdfea212e3a756a1fded7b35578525', '2022-08-14 15:02:53', '2022-04-01 14:19:25', '2022-04-01 14:19:25', 0);
INSERT INTO `t_customs` VALUES (4, '月半节', '又称“鬼节”、“亡人节”，时间为农历七月十二，土家人素有“年小月半大”之说。其主要活动是祭祖，祭祖时烧纸钱，表示对已故亲人的哀悼。过月半须全家团聚，并接回出嫁的女儿。', 1108516, 'image/studyroom/text_1649487932896da54dd5a0398011cdfa50d559c2c0ef8', '2022-08-12 15:05:24', '2022-04-09 15:05:33', '2022-04-09 15:05:33', 0);
INSERT INTO `t_customs` VALUES (5, '祝米', '土家族人家生了孩子，必会对小生命的出世举行热烈庆典，俗称“整祝米酒”。时间定于“洗三”那天，或定于满月之际。无论地位贵贱，婴儿的外祖母在这天都将享受到上等贵客的殊荣。', 1108517, 'image/studyroom/text_16494880132152ba2520186ee376e835ce7bf1554ef7b', '1970-01-01 08:00:00', '2022-04-09 23:06:53', '2022-04-09 23:06:53', 0);
INSERT INTO `t_customs` VALUES (6, '哭嫁', '土家族女儿出嫁之前要哭嫁。是土家族婚俗中最富有人情韵味的文化特色。哭嫁本是对封建礼教取代土家族自由婚俗的一种反叛，后来，逐渐形成了以悲言喜的文化形态。（哭嫁完全是一种热热闹闹的爱的庆典形式）', 1108518, 'image/studyroom/text_1649488066113db2de541293171af2b0ccdf7c64d72d4', '2022-05-27 15:07:19', '2022-04-09 15:07:46', '2022-04-09 15:07:46', 0);

-- ----------------------------
-- Table structure for t_food
-- ----------------------------
DROP TABLE IF EXISTS `t_food`;
CREATE TABLE `t_food`  (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '食物名称',
  `description` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '食物描述',
  `image_id` int NULL DEFAULT NULL COMMENT '图片id',
  `image` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图片',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_delete` tinyint NOT NULL DEFAULT 0,
  `address_id` int NOT NULL DEFAULT 1 COMMENT '食物所在地',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_food
-- ----------------------------
INSERT INTO `t_food` VALUES (3, '豆皮', '恩施豆皮是湖北恩施州的一种地方特色小吃，主要制作材料是土豆、玉米、大米，成品色泽乳白，有淡淡香味。其做法主要有两种，一种是煮，一种是炒，如今它已经成为一种旅游食品和民间早点。', 1108508, 'image/studyroom/text_164948669274153c04118df112c13a8c34b38343b9c10', '2022-03-17 15:01:23', '2022-03-17 15:09:45', 0, 2);
INSERT INTO `t_food` VALUES (4, '炕洋芋', '恩施炕土豆是恩施的特产之一，首先把土豆洗净，然后切条或切块，放入热油锅中，待到金黄色时，土豆就基本完成了，然后放入大盘中，加入调料：姜汁、蒜汁、腐乳汁、黄豆酱、花椒、盐、辣椒等,最后放上一点香菜。然后搅拌，就这样，一盘香喷喷的炕土豆就完成了。\r\n炕土豆的营养价值很高，还可减肥，还有健胃养脾的作用，而且还可以预防中风。', 1108503, 'image/studyroom/text_16494857771138e2c381d4dd04f1c55093f22c59c3a08', '2022-04-01 11:29:26', '2022-04-01 11:29:26', 0, 1);
INSERT INTO `t_food` VALUES (5, '1', '2', 1108498, 'image/studyroom/text_1648710038307d5036c64412973d610202be8dce2b82a', '2022-03-31 23:00:38', '2022-03-31 23:00:38', 1, 1);
INSERT INTO `t_food` VALUES (6, '腊肉', '家家都兴喂年猪，主要是图过年时有肉吃，过年吃不完的，土家人便把它制作成腊肉，不仅便于保存，而且肉色更加好看，把腊肉放在锅里烹煮，香飘十里，勾人食欲，是土家人平时招待客人摆在席上的主菜。', 1108504, 'image/studyroom/text_164948608737768a83eeb494a308fe5295da69428a507', '2022-04-09 14:34:47', '2022-04-09 14:34:47', 0, 1);
INSERT INTO `t_food` VALUES (7, '玉露茶', '恩施玉露，湖北省恩施市特产，中国国家地理标志产品。 \r\n恩施玉露外形条索紧圆光滑、纤细挺直如针，色泽苍翠绿润。经沸水冲泡，芽叶复展如生，初时婷婷地悬浮杯中，继而沉降杯底，平伏完整，汤色嫩绿明亮，如玉露，香气清爽，滋味醇和。观其外形，赏心悦目；饮其茶汤，沁人心脾。 \r\n2007年03月05日，原国家质检总局批准对“恩施玉露”实施地理标志产品保护', 1108505, 'image/studyroom/text_164948626549779cc30c73507cfc25d20fe7f7bcfd91b', '2022-04-09 14:37:45', '2022-04-09 14:37:45', 0, 1);
INSERT INTO `t_food` VALUES (8, '土家油香', '恩施油香，又称油粑粑，江浙一带称之为灯盏糕，因其像旧时陶瓷灯盏的底盘而得名。明清时期经迁移恩施的军人与商人传带过来。在恩施，油香制作技艺已有100多年的历史。', 1108506, 'image/studyroom/text_164948639875189f0fd5c927d466d6ec9a21b9ac34ffa', '2022-04-09 14:39:58', '2022-04-09 14:39:58', 0, 1);
INSERT INTO `t_food` VALUES (9, '苞谷饭', '恩施地区多产包谷和大米，恩施土家人将这两种拌在一起的食物叫做“包谷饭”。包谷饭也叫“金包银”，其实就是玉米面裹上白米饭。土家包谷饭营养丰富、松软味香，其颜色鲜亮，黄里透白，被土家人视为“吉利饭”。', 1108507, 'image/studyroom/text_1649486636367d4c2e4a3297fe25a71d030b67eb83bfc', '2022-04-09 14:43:56', '2022-04-09 14:43:56', 0, 1);
INSERT INTO `t_food` VALUES (10, '建始大饼', '土家女儿饼原名建始大饼，又名唐氏土家女儿饼，恩施土家女儿饼。是湖北恩施州土家族特有的小吃，2014年经恩施新峰贸易公司改良推广至今深受好评，土家女儿饼特有的口味，迅速火遍全国。土家女儿饼以其醉人的自然清香、松腴柔润的口感、色泽金黄鲜明而让人们的味蕾绽放，得以在这个时代盛行。一个个金黄香脆的女儿饼就是一个个色美味佳的艺术品，让人望之能起三分馋，闻之则添七分饥。', 1108509, 'image/studyroom/text_164948734545264de166633d61c8326232568b42beef1', '2022-04-09 14:55:45', '2022-04-09 14:55:45', 0, 2);
INSERT INTO `t_food` VALUES (11, '关口葡萄', '关口葡萄仅在建始县花坪镇关口乡村坊村（小地名关口，位于景阳关（建平关）附近）生长，故名。果粒着生中等紧凑，每穗平均着粒数为40-60粒，果粒呈卵圆形或近圆形，果皮较薄，其色泽绿而透明，有如碧玉；其果香四溢，果肉柔软多汁，可容性固含物为16.5—18%，味甜可口。', 1108510, 'image/studyroom/text_16494873946045227b6aaf294f5f027273aebf16015f2', '2022-04-09 14:56:34', '2022-04-09 14:56:34', 0, 2);
INSERT INTO `t_food` VALUES (12, '花坪桃片糕', '花坪桃片糕作为一种年节美食，能岁岁为人们带来新的希望。年糕又称\"年年糕\"，与\"年年高\"谐音，寄寓了农耕时代人们对生产与生活的美好祝福与祈求。花坪桃片糕为百年老字号产品，其制作工艺复杂，产品受人喜欢。由于本产品起源于吴氏家庭，长期由世代吴姓人掌握核心技术，从而也保证了桃片糕生产的纯正性和传承性。 2011年，该项目被省人民政府公布为省级非物质文化遗产名录。', 1108511, 'image/studyroom/text_164948746244805128e44e27c36bdba71221bfccf735d', '2022-04-09 14:57:42', '2022-04-09 14:57:42', 0, 2);
INSERT INTO `t_food` VALUES (13, '景阳河核桃', '景阳核桃是湖北省恩施州建始县景阳镇的特产。建始景阳核桃具有果大、壳薄、肉丰且嫩、味美、出仁率高、含脂高等特点，多项指标达到了国际标准，是馈赠亲友和滋补身体的上乘佳品，深受消费者的青睐。\r\n　　景阳核桃主要生长在清江峡谷地带海拔800米以下的10多个村。按照核桃适宜在1000多米以上的生长环境看，景阳不具备核桃树生长的条件，是其特殊的气候条件孕育了特色产品。', 1108512, 'image/studyroom/text_16494875095688d6a06b2f1208b59454a9a749928b0c0', '2022-04-09 14:58:30', '2022-04-09 14:58:30', 0, 2);
INSERT INTO `t_food` VALUES (14, '公婆饼', '“土家公婆饼”又称“湖北公婆饼”、“恩施公婆饼”、“恩施土家族公婆饼”、“武汉公婆饼”简称“公婆饼”。它是土家族特有的风味美食，采用发酵面团包卷由几十种天然佐料调制而成的新鲜猪肉馅料，辅一葱花，沾一芝麻、用电饼铛煎制而成。', 1108519, 'image/studyroom/text_16494881412166600e06fe9350b62c1e343504d4a7b86', '2022-04-09 15:09:01', '2022-04-09 15:09:01', 0, 1);

-- ----------------------------
-- Table structure for t_food_address
-- ----------------------------
DROP TABLE IF EXISTS `t_food_address`;
CREATE TABLE `t_food_address`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `address_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '地名',
  `is_delete` tinyint NOT NULL DEFAULT 0,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_food_address
-- ----------------------------
INSERT INTO `t_food_address` VALUES (1, '恩施', 0, '2022-03-13 14:54:09', '2022-03-13 14:54:09');
INSERT INTO `t_food_address` VALUES (2, '建始', 0, '2022-03-13 15:16:47', '2022-03-13 15:16:47');

-- ----------------------------
-- Table structure for t_image
-- ----------------------------
DROP TABLE IF EXISTS `t_image`;
CREATE TABLE `t_image`  (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `file_name` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '文件名',
  `width` int NOT NULL COMMENT '宽',
  `height` int NOT NULL COMMENT '高',
  `is_gif` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否gif',
  `bucket` tinyint NOT NULL DEFAULT 0 COMMENT '文件在阿里云存的bucket位置',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '上传时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1108521 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '基本图片表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_image
-- ----------------------------
INSERT INTO `t_image` VALUES (1108491, 'image/studyroom/text_16472228402421138d90ef0a0848a542e57d1595f58ea', 300, 300, 0, 0, '2022-03-14 09:53:59', '2022-03-14 09:53:59');
INSERT INTO `t_image` VALUES (1108492, 'image/studyroom/text_164722324243897af4fb322bb5c8973ade16764156bed', 536, 536, 0, 0, '2022-03-14 10:00:41', '2022-03-14 10:00:41');
INSERT INTO `t_image` VALUES (1108493, 'image/studyroom/text_1647223598046d759175de8ea5b1d9a2660e45554894f', 536, 536, 0, 0, '2022-03-14 10:06:36', '2022-03-14 10:06:36');
INSERT INTO `t_image` VALUES (1108494, 'image/studyroom/text_16474139652234ba29b9f9e5732ed33761840f4ba6c53', 500, 282, 0, 0, '2022-03-16 14:59:25', '2022-03-16 14:59:25');
INSERT INTO `t_image` VALUES (1108495, 'image/studyroom/text_1647414083327825f9cd5f0390bc77c1fed3c94885c87', 500, 282, 0, 0, '2022-03-16 15:01:23', '2022-03-16 15:01:23');
INSERT INTO `t_image` VALUES (1108496, 'image/studyroom/text_16474277939057274a60c83145b1082be9caa91926ecf', 633, 376, 0, 0, '2022-03-16 18:49:53', '2022-03-16 18:49:53');
INSERT INTO `t_image` VALUES (1108497, 'image/studyroom/text_1648668566785f76a89f0cb91bc419542ce9fa43902dc', 557, 300, 0, 0, '2022-03-31 03:29:26', '2022-03-31 03:29:26');
INSERT INTO `t_image` VALUES (1108498, 'image/studyroom/text_1648710038307d5036c64412973d610202be8dce2b82a', 557, 300, 0, 0, '2022-03-31 15:00:38', '2022-03-31 15:00:38');
INSERT INTO `t_image` VALUES (1108499, 'image/studyroom/text_1648725385003a24281a03c28fa405eb29b54ebfe5d9b', 557, 300, 0, 0, '2022-03-31 19:16:25', '2022-03-31 19:16:25');
INSERT INTO `t_image` VALUES (1108500, 'image/studyroom/text_164873610658016c244e39517c1e577a0b778cb1ce568', 557, 300, 0, 0, '2022-03-31 22:15:06', '2022-03-31 22:15:06');
INSERT INTO `t_image` VALUES (1108501, 'image/studyroom/text_1648736203712614594c34e0c9dc796cb21d5e806768b', 557, 300, 0, 0, '2022-03-31 22:16:43', '2022-03-31 22:16:43');
INSERT INTO `t_image` VALUES (1108502, 'image/studyroom/text_1648736365894fc322c6bd467dc6e4a70ece4ce0245f8', 557, 300, 0, 0, '2022-03-31 22:19:25', '2022-03-31 22:19:25');
INSERT INTO `t_image` VALUES (1108503, 'image/studyroom/text_16494857771138e2c381d4dd04f1c55093f22c59c3a08', 633, 474, 0, 0, '2022-04-09 14:29:37', '2022-04-09 14:29:37');
INSERT INTO `t_image` VALUES (1108504, 'image/studyroom/text_164948608737768a83eeb494a308fe5295da69428a507', 396, 396, 0, 0, '2022-04-09 14:34:47', '2022-04-09 14:34:47');
INSERT INTO `t_image` VALUES (1108505, 'image/studyroom/text_164948626549779cc30c73507cfc25d20fe7f7bcfd91b', 317, 205, 0, 0, '2022-04-09 14:37:45', '2022-04-09 14:37:45');
INSERT INTO `t_image` VALUES (1108506, 'image/studyroom/text_164948639875189f0fd5c927d466d6ec9a21b9ac34ffa', 578, 385, 0, 0, '2022-04-09 14:39:58', '2022-04-09 14:39:58');
INSERT INTO `t_image` VALUES (1108507, 'image/studyroom/text_1649486636367d4c2e4a3297fe25a71d030b67eb83bfc', 640, 424, 0, 0, '2022-04-09 14:43:56', '2022-04-09 14:43:56');
INSERT INTO `t_image` VALUES (1108508, 'image/studyroom/text_164948669274153c04118df112c13a8c34b38343b9c10', 633, 334, 0, 0, '2022-04-09 14:44:52', '2022-04-09 14:44:52');
INSERT INTO `t_image` VALUES (1108509, 'image/studyroom/text_164948734545264de166633d61c8326232568b42beef1', 633, 361, 0, 0, '2022-04-09 14:55:45', '2022-04-09 14:55:45');
INSERT INTO `t_image` VALUES (1108510, 'image/studyroom/text_16494873946045227b6aaf294f5f027273aebf16015f2', 633, 356, 0, 0, '2022-04-09 14:56:34', '2022-04-09 14:56:34');
INSERT INTO `t_image` VALUES (1108511, 'image/studyroom/text_164948746244805128e44e27c36bdba71221bfccf735d', 633, 421, 0, 0, '2022-04-09 14:57:42', '2022-04-09 14:57:42');
INSERT INTO `t_image` VALUES (1108512, 'image/studyroom/text_16494875095688d6a06b2f1208b59454a9a749928b0c0', 700, 700, 0, 0, '2022-04-09 14:58:29', '2022-04-09 14:58:29');
INSERT INTO `t_image` VALUES (1108513, 'image/studyroom/text_1649487614921819f46e52c25763a55cc642422644317', 633, 422, 0, 0, '2022-04-09 15:00:15', '2022-04-09 15:00:15');
INSERT INTO `t_image` VALUES (1108514, 'image/studyroom/text_16494876611701f9702dbc66344013ffb884419665816', 633, 376, 0, 0, '2022-04-09 15:01:01', '2022-04-09 15:01:01');
INSERT INTO `t_image` VALUES (1108515, 'image/studyroom/text_1649487821534ebbdfea212e3a756a1fded7b35578525', 1080, 596, 0, 0, '2022-04-09 15:03:42', '2022-04-09 15:03:42');
INSERT INTO `t_image` VALUES (1108516, 'image/studyroom/text_1649487932896da54dd5a0398011cdfa50d559c2c0ef8', 865, 568, 0, 0, '2022-04-09 15:05:33', '2022-04-09 15:05:33');
INSERT INTO `t_image` VALUES (1108517, 'image/studyroom/text_16494880132152ba2520186ee376e835ce7bf1554ef7b', 1120, 747, 0, 0, '2022-04-09 15:06:53', '2022-04-09 15:06:53');
INSERT INTO `t_image` VALUES (1108518, 'image/studyroom/text_1649488066113db2de541293171af2b0ccdf7c64d72d4', 547, 366, 0, 0, '2022-04-09 15:07:46', '2022-04-09 15:07:46');
INSERT INTO `t_image` VALUES (1108519, 'image/studyroom/text_16494881412166600e06fe9350b62c1e343504d4a7b86', 800, 600, 0, 0, '2022-04-09 15:09:01', '2022-04-09 15:09:01');
INSERT INTO `t_image` VALUES (1108520, 'image/studyroom/text_1678270188942f8ea2e8463760785106490befc78c339', 2276, 1280, 0, 0, '2023-03-08 18:09:47', '2023-03-08 18:09:47');

-- ----------------------------
-- Table structure for t_user
-- ----------------------------
DROP TABLE IF EXISTS `t_user`;
CREATE TABLE `t_user`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'pk\r\n',
  `username` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '用户名',
  `phone` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '' COMMENT '手机号',
  `image_id` int NOT NULL DEFAULT 0 COMMENT '头像id，basic_image id',
  `image_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '图片url',
  `admin` tinyint NOT NULL DEFAULT 0 COMMENT '是否是管理员 0 是 1 不是',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_username`(`username` ASC) USING BTREE,
  INDEX `idx_mobile`(`phone` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_user
-- ----------------------------
INSERT INTO `t_user` VALUES (1, 'admin', '18772916901', 0, '', 0, '2022-03-13 12:16:45', '2022-03-13 12:16:45');
INSERT INTO `t_user` VALUES (2, NULL, '18694009651', 0, '', 0, '2022-03-28 16:07:45', '2022-03-28 16:07:45');
INSERT INTO `t_user` VALUES (3, NULL, '', 0, '', 0, '2022-03-28 23:54:36', '2022-03-28 23:54:36');
INSERT INTO `t_user` VALUES (4, NULL, '17786438357', 0, '', 0, '2022-04-08 21:29:09', '2022-04-08 21:29:09');

-- ----------------------------
-- Table structure for t_view
-- ----------------------------
DROP TABLE IF EXISTS `t_view`;
CREATE TABLE `t_view`  (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '景点名称',
  `address` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '景点地址',
  `description` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '景点详情',
  `image_id` int NULL DEFAULT NULL COMMENT '图片id',
  `image` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图片',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_delete` tinyint NOT NULL DEFAULT 0,
  `level_id` int NOT NULL DEFAULT 0 COMMENT '景区等级id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_view
-- ----------------------------
INSERT INTO `t_view` VALUES (1, '恩施大峡谷 ', '湖北省恩施土家族苗族自治州恩施市019乡道', '恩施大峡谷，峡谷全长108千米，面积达300平方千米。不偏不倚落脚于神秘的“北纬30度”，距恩施市区49公里，距利川市区39公里。景区先后被评为国家AAAAA级旅游景区、国家地质公园，灵秀湖北的十大旅游名片之一。', 1108496, 'image/studyroom/text_16474277939057274a60c83145b1082be9caa91926ecf', '2022-03-16 18:49:53', '2022-03-16 18:49:53', 0, 5);
INSERT INTO `t_view` VALUES (2, '11', '22', '33', 1108499, 'image/studyroom/text_1648725385003a24281a03c28fa405eb29b54ebfe5d9b', '2022-04-01 11:16:25', '2022-04-01 11:16:25', 1, 5);
INSERT INTO `t_view` VALUES (3, '女儿城', '湖北省恩施土家族苗族自治州恩施市马鞍山路41号', '世间男子不二心，天下女儿第一城！中国恩施土家女儿城，位于湖北省恩施市区七里坪，是全国土家族文化集聚地，也是武陵地区城市娱乐消费中心和旅游集散地。\r\n作为全国第八个人造古镇，土家女儿城合理且精心的谋划了整体建筑风格，仿古与土家吊脚楼相结合，完美的体现了土家族的民风民俗', 1108513, 'image/studyroom/text_1649487614921819f46e52c25763a55cc642422644317', '2022-04-09 23:00:15', '2022-04-09 23:00:15', 0, 4);
INSERT INTO `t_view` VALUES (4, '地心谷', '湖北省恩施土家族苗族自治州建始县高坪镇G318', '湖北恩施有个地心谷，是国家AAAA级景区。地心谷属世界珍奇高山喀斯特岩溶嶂谷地貌，被美国CNN和中国国家地理点名，评为“中国最美仙境”。地心谷景区9D高空玻璃悬索桥，横跨大峡谷，凌空两山之间。桥长268米，宽3.2米，桥面距谷底约200米。从玻璃悬索天桥进入景区，脚下玻璃会出现出现裂破纹的视听觉冲击，心惊胆颤路在桥上自上而下俯视地心谷底，如同在空中鸟瞰，有一种即将穿越的神秘之感。', 1108514, 'image/studyroom/text_16494876611701f9702dbc66344013ffb884419665816', '2022-04-09 23:01:01', '2022-04-09 23:01:01', 0, 4);

-- ----------------------------
-- Table structure for t_view_level
-- ----------------------------
DROP TABLE IF EXISTS `t_view_level`;
CREATE TABLE `t_view_level`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `level_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '4AAAA级景区',
  `is_delete` tinyint NOT NULL DEFAULT 0,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_view_level
-- ----------------------------
INSERT INTO `t_view_level` VALUES (1, 'A级景区', 0, '2022-03-16 18:25:20', '2022-03-16 18:25:20');
INSERT INTO `t_view_level` VALUES (2, 'AA级景区', 0, '2022-03-16 18:25:29', '2022-03-16 18:25:50');
INSERT INTO `t_view_level` VALUES (3, 'AAA级景区', 0, '2022-03-16 18:25:46', '2022-03-16 18:25:55');
INSERT INTO `t_view_level` VALUES (4, 'AAAA级景区', 0, '2022-03-16 18:26:12', '2022-03-16 18:26:27');
INSERT INTO `t_view_level` VALUES (5, 'AAAAA级景区', 0, '2022-03-16 18:26:42', '2022-03-16 18:26:42');

SET FOREIGN_KEY_CHECKS = 1;
