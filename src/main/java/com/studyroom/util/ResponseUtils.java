package com.studyroom.util;

/**
 * @author Administrator
 */
public class ResponseUtils {

    public static <T> ResponseEntity<T> body(String errCode, String errMsg, T t) {
        return new ResponseEntity<>(errCode, errMsg, t);
    }

    public static <T> ResponseEntity<T> body(Boolean result,String errCode, String errMsg, T t) {
        return new ResponseEntity<>(result,errCode, errMsg, t);
    }

    public static <T> ResponseEntity<T> body(CommonResponseEnum commonResponseEnum, T t) {
        return new ResponseEntity<>(commonResponseEnum.getCode(), commonResponseEnum.getMessage(), t);
    }

    public static <T> ResponseEntity<T> success(CommonResponseEnum commonResponseEnum,T t) {
        return new ResponseEntity<>(true,commonResponseEnum.getCode(),commonResponseEnum.getMessage(),t);
    }

    public static <T> ResponseEntity<T> fail(CommonResponseEnum commonResponseEnum,T t) {
        return new ResponseEntity<>(false,commonResponseEnum.getCode(),commonResponseEnum.getMessage(),t);
    }
    public static <T> ResponseEntity<T> fail(String message) {
        return new ResponseEntity<>(false,"500",message,null);
    }

    public static <T> ResponseEntity<T> fail() {
        return fail(CommonResponseEnum.FAILED,null);
    }

    public static <T> ResponseEntity<T> success(T data) {
        return new ResponseEntity<>(true,"200","请求成功",data);
    }
}
