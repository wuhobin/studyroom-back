package com.studyroom.constant;

/**
 * @author wuhobin
 * @date 2021/12/5 16:30
 * @description
 */
public interface RedisConstantsKey {
    /**
     * 短信验证码缓存
     */
    String SMS_CODE_STRING = "SMS_CODE:%s";

    /**
     * 简介缓存
     */
    String CITY_DESCRIPTION = "city_description";
}
