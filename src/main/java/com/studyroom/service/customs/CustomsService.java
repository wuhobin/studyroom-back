package com.studyroom.service.customs;

import com.studyroom.util.ResponseEntity;
import com.studyroom.vo.admin.CustomsParamVo;
import com.studyroom.vo.admin.ViewParamVo;

/**
 * @author wuhongbin
 * @createTime 2022年03月16日 17:30:00
 */
public interface CustomsService {

    /**
     *
     * @param pageNum 1
     * @return 1
     */
    ResponseEntity<?> getCustomsList(Integer pageNum);

    /**
     *
     * @param paramVo 1
     * @return 1
     */
    ResponseEntity<?> addCustoms(CustomsParamVo paramVo);

    /**
     *
     * @param id 1
     * @return 1
     */
    ResponseEntity<?> deleteCustoms(Integer id);

    /**
     *
     * @param paramVo  1
     * @return  1
     */
    ResponseEntity<?> updateCustoms(CustomsParamVo paramVo);
}
