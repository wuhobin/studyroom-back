package com.studyroom.util;

import lombok.Getter;

/**
 * @author wuhongbin
 * 公用返回枚举
 */
@Getter
public enum CommonResponseEnum {
    /**
     * 返回成功
     */
    SUCCESS("200","请求成功"),
    /**
     * 操作成功
     */
    OPERATE_SUCCESS("200","操作成功"),
    /**
     * 操作成功
     */
    SYSTEM_BUSY("300","系统繁忙"),
    /**
     * 请求失败
     */
    FAIL("400","请求失败"),
    /**
     * 请求失败2
     */
    NO_DATA("400","暂无数据"),
    /**
     * 非法输入
     */
    ILLEGAL_INPUT("401","非法输入"),
    /**
     * 操作失败
     */
    OPERATE_FAIL("403", "操作失败"),
    /**
     * 操作失败
     */
    DELETE_FAIL("406", "删除失败"),
    /**
     * 系统内部错误
     */
    SYSTEM_ERROR("500","系统内部错误"),

    VALIDATE_FAILED("400", "参数检验失败"),

    REQUEST_METHOD_NOT_SUPPORTED("405", "请求方法不支持"),

    FAILED("500", "服务器开小差了")
    ;
    CommonResponseEnum(String code, String message) {
        this.code = code;
        this.message = message;
    }

    private final String code;
    private final String message;
}
