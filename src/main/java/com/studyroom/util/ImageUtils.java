package com.studyroom.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author wuhongbin
 * @createTime 2022年03月16日 15:05:00
 */
@Component
public class ImageUtils {

    @Value("${image.url}")
    private String imageUrl;

    public String getUrl(String filename){
        return imageUrl+filename;
    }
}
