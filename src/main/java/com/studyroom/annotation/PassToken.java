package com.studyroom.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author wuhongbin
 * @version 1.0.0
 * @ClassName PassToken.java
 * @Description 方法上拥有此注解可以跳过token验证
 * @createTime 2021年11月01日 18:38:00
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface PassToken {
    boolean required() default true;
}
