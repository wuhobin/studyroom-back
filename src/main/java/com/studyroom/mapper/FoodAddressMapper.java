package com.studyroom.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.studyroom.pojo.FoodAddress;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author wuhongbin
 * @version 1.0.0
 * @ClassName FoodAddressMapper.java
 * @Description TODO
 * @createTime 2022年03月13日 14:58:00
 */
@Repository
public interface FoodAddressMapper extends BaseMapper<FoodAddress> {

    @Select("SELECT id,address_name FROM t_food_address WHERE is_delete = 0")
    List<FoodAddress> getFoodAddress();


    @Select("SELECT id,address_name FROM t_food_address WHERE is_delete = 0 ORDER BY id ASC")
    List<FoodAddress> getFoodAddressOrder();
}
