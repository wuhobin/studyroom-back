package com.studyroom.service.food.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.studyroom.mapper.FoodAddressMapper;
import com.studyroom.mapper.FoodMapper;
import com.studyroom.pojo.Food;
import com.studyroom.pojo.FoodAddress;
import com.studyroom.service.food.FoodService;
import com.studyroom.service.oss.OssService;
import com.studyroom.util.ImageUtils;
import com.studyroom.util.ResponseEntity;
import com.studyroom.util.ResponseUtils;
import com.studyroom.vo.admin.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author wuhongbin
 * @createTime 2022年03月13日 14:05:00
 */
@Service
@Slf4j
public class FoodServiceImpl implements FoodService {

    private final FoodMapper foodMapper;
    private final FoodAddressMapper foodAddressMapper;
    private final OssService ossService;
    private final ImageUtils imageUtils;

    @Autowired
    public FoodServiceImpl(FoodMapper foodMapper,FoodAddressMapper foodAddressMapper,OssService ossService,ImageUtils imageUtils){
        this.foodMapper = foodMapper;
        this.foodAddressMapper = foodAddressMapper;
        this.ossService = ossService;
        this.imageUtils = imageUtils;
    }

    @Override
    public ResponseEntity<?> getFoodList(Integer pageNum) {
        if (Objects.isNull(pageNum) || pageNum < 1){
            pageNum = 1;
        }
        List<FoodAddress> foodAddress = foodAddressMapper.getFoodAddress();
        Map<Integer, FoodAddress> foodAddressMap = foodAddress.stream().collect(Collectors.toMap(FoodAddress::getId, Function.identity()));
        Integer count = foodMapper.selectCount(new QueryWrapper<Food>().lambda().eq(Food::getIsDelete, 0));
        List<Food> foodList = foodMapper.getFoodList(pageNum, (pageNum - 1) * 10, 10);
        LinkedList<FoodListVo> foodListVos = new LinkedList<>();
        foodList.forEach(item ->{
            FoodListVo foodListVo = FoodListVo.builder()
                    .id(item.getId())
                    .foodAddress(foodAddressMap.get(item.getAddressId()).getAddressName())
                    .name(item.getName())
                    .description(item.getDescription())
                    .imageUrl(imageUtils.getUrl(item.getImage()))
                    .createTime(item.getCreateTime()).build();
            foodListVos.add(foodListVo);
        });
        PageInfoVo pageInfoVo = PageInfoVo.<FoodListVo>builder()
                .count(count)
                .pages(count / 10 + 1)
                .data(foodListVos)
                .build();
        return ResponseUtils.success(pageInfoVo);
    }

    @Override
    public ResponseEntity<?> getAddressNameList() {
        List<FoodAddress> foodAddress = foodAddressMapper.getFoodAddressOrder();
        LinkedList<FoodAddressVo> foodAddressVos = new LinkedList<>();
        foodAddress.forEach(item -> {
            FoodAddressVo foodAddressVo = FoodAddressVo.builder()
                    .id(item.getId())
                    .addressName(item.getAddressName())
                    .build();
            foodAddressVos.add(foodAddressVo);
        });
        return ResponseUtils.success(foodAddressVos);
    }

    @Override
    public ResponseEntity<?> deleteFood(Integer id) {
        Food food = foodMapper.selectById(id);
        if (ObjectUtils.isEmpty(food)){
            return ResponseUtils.fail("该食品不存在");
        }
        food.setIsDelete(1);
        foodMapper.updateById(food);
        return ResponseUtils.success("删除成功");
    }

    @Override
    public ResponseEntity<?> addFood(FoodParamVo paramVo) {
        String name = paramVo.getName();
        Food food = foodMapper.selectOne(new QueryWrapper<Food>().lambda().eq(Food::getName, name).eq(Food::getIsDelete, 0));
        if (!ObjectUtils.isEmpty(food)){
            return ResponseUtils.fail("已存在该食品");
        }
        OssResultVo foodImage = null;
        if (!ObjectUtils.isEmpty(paramVo.getImage())){
            foodImage = ossService.uploadImage(paramVo.getImage());
        }
        food = new Food();
        food.setName(name);
        food.setDescription(paramVo.getDescription());
        food.setIsDelete(0);
        food.setImageId(foodImage.getImageId());
        food.setImage(foodImage.getImageName());
        food.setAddressId(paramVo.getAddressId());
        foodMapper.insert(food);
        return ResponseUtils.success("添加成功");
    }

    @Override
    public ResponseEntity<?> updateFood(FoodParamVo paramVo) {
        Integer id = paramVo.getId();
        if (id == null){
            return ResponseUtils.fail("食品id不能为null");
        }
        Food food = foodMapper.selectOne(new QueryWrapper<Food>().lambda().eq(Food::getId, id).eq(Food::getIsDelete, 0));
        if (ObjectUtils.isEmpty(food)){
            return ResponseUtils.fail("该食品不存在");
        }
        if (!food.getName().equals(paramVo.getName())){
            Food foodOne = foodMapper.selectOne(new QueryWrapper<Food>().lambda().eq(Food::getName, paramVo.getName()).eq(Food::getIsDelete, 0));
            if (!ObjectUtils.isEmpty(foodOne)){
                return ResponseUtils.fail("该食品已存在");
            }
        }
        OssResultVo foodImage;
        if (paramVo.getImage() != null){
            foodImage = ossService.uploadImage(paramVo.getImage());
            food.setImageId(foodImage.getImageId());
            food.setImage(foodImage.getImageName());
        }
        food.setName(paramVo.getName());
        food.setDescription(paramVo.getDescription());
        food.setAddressId(paramVo.getAddressId());
        foodMapper.updateById(food);
        return ResponseUtils.success("修改成功");
    }
}
