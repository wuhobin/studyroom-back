package com.studyroom.vo.admin;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author wuhongbin
 * @createTime 2022年03月13日 14:12:00
 */
@Data
public class ViewParamVo {
    private Integer pageNum;

    private Integer id;

    private String  name;

    private String address;

    private String description;

    private MultipartFile image;

    private Integer levelId;
}
