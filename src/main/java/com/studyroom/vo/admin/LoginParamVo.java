package com.studyroom.vo.admin;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @author wuhongbin
 * @createTime 2022年03月13日 12:06:00
 */
@Data
public class LoginParamVo {
    @NotNull(message = "手机号不能为空")
    private String phone;
    @NotNull(message = "验证码不能为空")
    private String captcha;
}
