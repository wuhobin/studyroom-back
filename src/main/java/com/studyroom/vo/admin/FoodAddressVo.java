package com.studyroom.vo.admin;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Data;

/**
 * @author wuhongbin
 * @createTime 2022年03月13日 15:12:00
 */
@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class FoodAddressVo {
    private Integer id;
    private String addressName;
    private String levelName;
}
