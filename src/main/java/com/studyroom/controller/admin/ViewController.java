package com.studyroom.controller.admin;

import com.studyroom.annotation.PassToken;
import com.studyroom.service.food.FoodService;
import com.studyroom.service.view.ViewService;
import com.studyroom.util.ResponseEntity;
import com.studyroom.vo.admin.FoodParamVo;
import com.studyroom.vo.admin.ViewParamVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author wuhongbin
 * @createTime 2022年03月13日 14:04:00
 */
@RestController
@RequestMapping("/view")
public class ViewController {

    private final ViewService viewService;

    @Autowired
    public ViewController(ViewService viewService){
        this.viewService = viewService;
    }

    @PassToken
    @PostMapping("/list")
    public ResponseEntity<?> getViewList(@RequestBody ViewParamVo paramVo){
        return viewService.getViewList(paramVo.getPageNum());
    }

    @PassToken
    @GetMapping("/level-list")
    public ResponseEntity<?> getLevelNameList(){
        return viewService.getLevelNameList();
    }

    @PostMapping("/add")
    public ResponseEntity<?> addView(ViewParamVo paramVo){
        return viewService.addView(paramVo);
    }


    @GetMapping("/delete/{id}")
    public ResponseEntity<?> deleteView(@PathVariable("id") Integer id){
        return viewService.deleteView(id);
    }


    @PostMapping("/update")
    public ResponseEntity<?> updateView(ViewParamVo paramVo){
        return viewService.updateView(paramVo);
    }


}
