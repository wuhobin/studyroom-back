package com.studyroom.service.customs.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.studyroom.mapper.CustomsMapper;
import com.studyroom.mapper.ViewLevelMapper;
import com.studyroom.mapper.ViewMapper;
import com.studyroom.pojo.Customs;
import com.studyroom.pojo.View;
import com.studyroom.pojo.ViewLevel;
import com.studyroom.service.customs.CustomsService;
import com.studyroom.service.oss.OssService;
import com.studyroom.service.view.ViewService;
import com.studyroom.util.ImageUtils;
import com.studyroom.util.ResponseEntity;
import com.studyroom.util.ResponseUtils;
import com.studyroom.vo.admin.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author wuhongbin
 * @createTime 2022年03月16日 17:30:00
 */
@Service
public class CustomsServiceImpl implements CustomsService {

    private final ImageUtils imageUtils;

    private final OssService ossService;

    private final CustomsMapper customsMapper;

    @Autowired
    public CustomsServiceImpl(CustomsMapper customsMapper,ImageUtils imageUtils, OssService ossService){
        this.imageUtils = imageUtils;
        this.ossService = ossService;
        this.customsMapper = customsMapper;
    }


    @Override
    public ResponseEntity<?> getCustomsList(Integer pageNum) {
        if (Objects.isNull(pageNum) || pageNum < 1){
            pageNum = 1;
        }
        Integer count = customsMapper.selectCount(new QueryWrapper<Customs>().lambda().eq(Customs::getIsDelete, 0));
        List<Customs> customsList = customsMapper.getCustomsList((pageNum - 1) * 10, 10);
        LinkedList<CustomsListVo> customsListVos = new LinkedList<>();
        customsList.forEach(item ->{
            CustomsListVo customsListVo = CustomsListVo.builder()
                    .id(item.getId())
                    .time(item.getTime())
                    .name(item.getName())
                    .description(item.getDescription())
                    .imageUrl(imageUtils.getUrl(item.getImage()))
                    .createTime(item.getCreateTime()).build();
            customsListVos.add(customsListVo);
        });
        PageInfoVo pageInfoVo = PageInfoVo.<CustomsListVo>builder()
                .count(count)
                .pages(count / 10 + 1)
                .data(customsListVos)
                .build();
        return ResponseUtils.success(pageInfoVo);
    }

    @Override
    public ResponseEntity<?> addCustoms(CustomsParamVo paramVo) {
        String name = paramVo.getName();
        Customs customs = customsMapper.selectOne(new QueryWrapper<Customs>().lambda().eq(Customs::getName, name).eq(Customs::getIsDelete, 0));
        if (!ObjectUtils.isEmpty(customs)){
            return ResponseUtils.fail("已存在该节日");
        }
        OssResultVo customsImage = null;
        if (!ObjectUtils.isEmpty(paramVo.getImage())){
            customsImage = ossService.uploadImage(paramVo.getImage());
        }
        customs = new Customs();
        customs.setName(name);
        customs.setDescription(paramVo.getDescription());
        customs.setIsDelete(0);
        customs.setTime(paramVo.getTime());
        customs.setImageId(customsImage.getImageId());
        customs.setImage(customsImage.getImageName());
        customsMapper.insert(customs);
        return ResponseUtils.success("添加成功");
    }

    @Override
    public ResponseEntity<?> deleteCustoms(Integer id) {
        Customs customs = customsMapper.selectById(id);
        if (ObjectUtils.isEmpty(customs)){
            return ResponseUtils.fail("该节日不存在");
        }
        customs.setIsDelete(1);
        customsMapper.updateById(customs);
        return ResponseUtils.success("删除成功");
    }

    @Override
    public ResponseEntity<?> updateCustoms(CustomsParamVo paramVo) {
        Integer id = paramVo.getId();
        if (id == null){
            return ResponseUtils.fail("节日id不能为null");
        }
        Customs customs = customsMapper.selectOne(new QueryWrapper<Customs>().lambda().eq(Customs::getId, id).eq(Customs::getIsDelete, 0));
        if (ObjectUtils.isEmpty(customs)){
            return ResponseUtils.fail("该节日不存在");
        }
        if (!customs.getName().equals(paramVo.getName())){
            Customs customsOne = customsMapper.selectOne(new QueryWrapper<Customs>().lambda().eq(Customs::getName, paramVo.getName()).eq(Customs::getIsDelete, 0));
            if (!ObjectUtils.isEmpty(customsOne)){
                return ResponseUtils.fail("该节日已存在");
            }
        }
        OssResultVo customImage;
        if (paramVo.getImage() != null){
            customImage = ossService.uploadImage(paramVo.getImage());
            customs.setImageId(customImage.getImageId());
            customs.setImage(customImage.getImageName());
        }
        customs.setName(paramVo.getName());
        customs.setDescription(paramVo.getDescription());
        customs.setTime(paramVo.getTime());
        customsMapper.updateById(customs);
        return ResponseUtils.success("修改成功");
    }
}
