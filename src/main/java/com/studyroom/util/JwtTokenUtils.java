package com.studyroom.util;

import io.jsonwebtoken.*;
import org.springframework.beans.factory.annotation.Value;

import java.util.Date;
import java.util.Map;

/**
 * @author token工具类
 */
public class JwtTokenUtils {


    //jwt创建时间
    private static final String CLAIM_KEY_CREATED="created";
    private static String secret = "it-studyroom";
    private static Long expiration = 604800L;



    /**
     * 从token中获取登录用户名
     * @param token
     * @return
     */
    public static String getUserNameFromToken(String token){
        String username;
        try {
            Claims claims = getClaimsFromToken(token);
            //通过荷载 claims 就可以拿到用户名
            username = claims.getSubject();
        } catch (Exception e) {
            username = null;
        }
        return username;
    }

    /**
     * 判断token是否可以被刷新
     * 如果过期了就可以被刷新，如果没过期就不能被刷新
     * @param token
     * @return
     */
    public static boolean canRefresh(String token){
        return !isTokenExpired(token);
    }

    /**
     * 刷新token
     * @param token
     * @return
     */
    public static String refreshToken(String token){
        Claims claims = getClaimsFromToken(token);
        //将创建时间改成当前时间，就相当于去刷新了
        claims.put(CLAIM_KEY_CREATED,new Date());
        return generateToken(claims);
    }

    /**
     * 判断token是否失效
     * @param token
     * @return
     */
    public static boolean isTokenExpired(String token) {
        Date expireDate = getExpiredDateFromToken(token);
        //判断token时间是否是当前时间的前面 .before
        return expireDate.before(new Date());
    }

    /**
     * 从token中获取过期时间
     * @param token
     * @return
     */
    public static Date getExpiredDateFromToken(String token) {
        //从token里面获取荷载
        //因为token的过期时间有对应的数据,设置过的,荷载里面就有设置过的数据
        Claims claims = getClaimsFromToken(token);
        return claims.getExpiration();
    }


    /**
     * 从token中获取荷载
     * @param token
     * @return
     */
    public static Claims getClaimsFromToken(String token) {
        //拿到荷载
        Claims claims = null ;
        try {
            claims = Jwts.parser()
                    //签名
                    .setSigningKey(secret)
                    //密钥
                    .parseClaimsJws(token)
                    .getBody();
        } catch (ExpiredJwtException  e) {
            throw new RuntimeException("当前登录已过期, 请重新登录!");
        }catch (InvalidClaimException e){
            throw new RuntimeException("token荷载失效");
        }catch(Exception  e){
            throw new RuntimeException("token解析异常");
        }
        return claims;
    }

    /**
     * 根据荷载生成 JWT TOKEN
     * @param claims
     * @return
     */
    public static String generateToken(Map<String,Object> claims){
        return Jwts.builder()
                .setClaims(claims)
                .setIssuedAt(new Date())
                //失效时间
                .setExpiration(generateExpirationDate())
                //签名
                .signWith(SignatureAlgorithm.HS512, secret)
                .compact();
    }

    /**
     * 生成token失效时间
     * @return
     */
    public static Date generateExpirationDate() {
        return new Date(System.currentTimeMillis() + expiration * 1000);
    }
}
