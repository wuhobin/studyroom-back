package com.studyroom.service.user;

import com.studyroom.pojo.User;
import com.studyroom.util.ResponseEntity;
import com.studyroom.vo.admin.LoginParamVo;
import org.springframework.stereotype.Service;

/**
 * @author wuhongbin
 * @version 1.0.0
 * @ClassName UserService.java
 * @Description 用户业务
 * @createTime 2021年11月02日 10:15:00
 */
public interface UserService {

    /**
     * 注册
     * @param user
     * @return
     */
    int registerUser(User user);


    /**
     * 根据手机号查找用户
     * @param phone
     * @return
     */
    User selectUserByPhone(String phone);

    /**
     * 根据手机号判断用户是否存在
     * @param phone
     * @return
     */
    Boolean isUserExist(String phone);


    /**
     * 手机号验证码登录
     * @param paramVo :
     * @return null
     */
    ResponseEntity<?> loginByCaptcha(LoginParamVo paramVo);
}
