package com.studyroom.vo;

import lombok.Data;

/**
 * @author wuhongbin
 * @version 1.0.0
 * @ClassName QqModel.java
 * @Description qq登录返回实体类
 * @createTime 2021年10月31日 16:09:00
 */
@Data
public class QqModel {
    private int ret;
    private String nickname;
    private String gender;
    private String figureurl_qq_1;
}
