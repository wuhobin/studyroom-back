package com.studyroom.service.view.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.studyroom.mapper.ViewLevelMapper;
import com.studyroom.mapper.ViewMapper;
import com.studyroom.pojo.Food;
import com.studyroom.pojo.FoodAddress;
import com.studyroom.pojo.View;
import com.studyroom.pojo.ViewLevel;
import com.studyroom.service.oss.OssService;
import com.studyroom.service.view.ViewService;
import com.studyroom.util.ImageUtils;
import com.studyroom.util.ResponseEntity;
import com.studyroom.util.ResponseUtils;
import com.studyroom.vo.admin.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author wuhongbin
 * @createTime 2022年03月16日 17:30:00
 */
@Service
public class ViewServiceImpl implements ViewService {

    private final ViewMapper viewMapper;

    private final ViewLevelMapper viewLevelMapper;

    private final ImageUtils imageUtils;

    private final OssService ossService;

    @Autowired
    public ViewServiceImpl(ViewMapper viewMapper,ViewLevelMapper viewLevelMapper,ImageUtils imageUtils,OssService ossService){
        this.viewMapper = viewMapper;
        this.viewLevelMapper = viewLevelMapper;
        this.imageUtils = imageUtils;
        this.ossService = ossService;
    }


    @Override
    public ResponseEntity<?> getViewList(Integer pageNum) {
        if (Objects.isNull(pageNum) || pageNum < 1){
            pageNum = 1;
        }
        List<ViewLevel> viewLevel = viewLevelMapper.getViewLevel();
        Map<Integer, ViewLevel> viewLevelMap = viewLevel.stream().collect(Collectors.toMap(ViewLevel::getId, Function.identity()));
        Integer count = viewMapper.selectCount(new QueryWrapper<View>().lambda().eq(View::getIsDelete, 0));
        List<View> viewList = viewMapper.getViewList((pageNum - 1) * 10, 10);
        LinkedList<ViewListVo> viewListVos = new LinkedList<>();
        viewList.forEach(item ->{
            ViewListVo viewListVo = ViewListVo.builder()
                    .id(item.getId())
                    .address(item.getAddress())
                    .name(item.getName())
                    .description(item.getDescription())
                    .imageUrl(imageUtils.getUrl(item.getImage()))
                    .level(viewLevelMap.get(item.getLevelId()).getLevelName())
                    .createTime(item.getCreateTime()).build();
            viewListVos.add(viewListVo);
        });
        PageInfoVo pageInfoVo = PageInfoVo.<ViewListVo>builder()
                .count(count)
                .pages(count / 10 + 1)
                .data(viewListVos)
                .build();
        return ResponseUtils.success(pageInfoVo);
    }

    @Override
    public ResponseEntity<?> addView(ViewParamVo paramVo) {
        String name = paramVo.getName();
        View view = viewMapper.selectOne(new QueryWrapper<View>().lambda().eq(View::getName, name).eq(View::getIsDelete, 0));
        if (!ObjectUtils.isEmpty(view)){
            return ResponseUtils.fail("已存在该景点");
        }
        OssResultVo viewImage = null;
        if (!ObjectUtils.isEmpty(paramVo.getImage())){
            viewImage = ossService.uploadImage(paramVo.getImage());
        }
        view = new View();
        view.setName(name);
        view.setDescription(paramVo.getDescription());
        view.setIsDelete(0);
        view.setAddress(paramVo.getAddress());
        view.setImageId(viewImage.getImageId());
        view.setImage(viewImage.getImageName());
        view.setLevelId(paramVo.getLevelId());
        viewMapper.insert(view);
        return ResponseUtils.success("添加成功");
    }

    @Override
    public ResponseEntity<?> deleteView(Integer id) {
        View view = viewMapper.selectById(id);
        if (ObjectUtils.isEmpty(view)){
            return ResponseUtils.fail("该景点不存在");
        }
        view.setIsDelete(1);
        viewMapper.updateById(view);
        return ResponseUtils.success("删除成功");
    }

    @Override
    public ResponseEntity<?> updateView(ViewParamVo paramVo) {
        Integer id = paramVo.getId();
        if (id == null){
            return ResponseUtils.fail("景点id不能为null");
        }
        View view = viewMapper.selectOne(new QueryWrapper<View>().lambda().eq(View::getId, id).eq(View::getIsDelete, 0));
        if (ObjectUtils.isEmpty(view)){
            return ResponseUtils.fail("该景点不存在");
        }
        if (!view.getName().equals(paramVo.getName())){
            View viewOne = viewMapper.selectOne(new QueryWrapper<View>().lambda().eq(View::getName, paramVo.getName()).eq(View::getIsDelete, 0));
            if (!ObjectUtils.isEmpty(viewOne)){
                return ResponseUtils.fail("该景点已存在");
            }
        }
        OssResultVo viewImage;
        if (paramVo.getImage() != null){
            viewImage = ossService.uploadImage(paramVo.getImage());
            view.setImageId(viewImage.getImageId());
            view.setImage(viewImage.getImageName());
        }
        view.setName(paramVo.getName());
        view.setDescription(paramVo.getDescription());
        view.setAddress(paramVo.getAddress());
        view.setLevelId(paramVo.getLevelId());
        viewMapper.updateById(view);
        return ResponseUtils.success("修改成功");
    }

    @Override
    public ResponseEntity<?> getLevelNameList() {
        List<ViewLevel> viewLevel = viewLevelMapper.getViewLevel();
        LinkedList<FoodAddressVo> foodAddressVos = new LinkedList<>();
        viewLevel.forEach(item -> {
            FoodAddressVo foodAddressVo = FoodAddressVo.builder()
                    .id(item.getId())
                    .levelName(item.getLevelName())
                    .build();
            foodAddressVos.add(foodAddressVo);
        });
        return ResponseUtils.success(foodAddressVos);
    }
}
