package com.studyroom.vo.admin;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author wuhongbin
 * @createTime 2022年03月13日 14:28:00
 */
@Data
@Builder
public class CustomsListVo {
    private Integer id;
    private String time;
    private String name;
    private String description;
    private String imageUrl;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;
}
