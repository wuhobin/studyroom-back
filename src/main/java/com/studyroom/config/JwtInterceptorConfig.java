package com.studyroom.config;

import com.studyroom.interceptor.JwtTokenInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author wuhongbin
 * @version 1.0.0
 * @ClassName JwtInterceptorConfig.java
 * @Description TODO
 * @createTime 2021年11月01日 19:13:00
 */
@Configuration
public class JwtInterceptorConfig implements WebMvcConfigurer {
    @Override
    public void addInterceptors(InterceptorRegistry registry) {

        //默认拦截所有路径
        registry.addInterceptor(authenticationInterceptor())
                .addPathPatterns("/**");
    }
    @Bean
    public JwtTokenInterceptor authenticationInterceptor() {
        return new JwtTokenInterceptor();
    }
}
