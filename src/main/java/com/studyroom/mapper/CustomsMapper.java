package com.studyroom.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.studyroom.pojo.Customs;
import com.studyroom.pojo.View;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author wuhongbin
 * @createTime 2022年03月13日 11:58:00
 */
@Repository
public interface CustomsMapper extends BaseMapper<Customs> {

    @Select(" SELECT * FROM t_customs WHERE is_delete = 0 ORDER BY id DESC LIMIT #{offset}, #{limit} ")
    List<Customs> getCustomsList(@Param("offset") Integer offset, @Param("limit") Integer limit);
}
