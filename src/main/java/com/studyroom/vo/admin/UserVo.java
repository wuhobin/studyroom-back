package com.studyroom.vo.admin;

import lombok.Builder;
import lombok.Data;

/**
 * @author wuhongbin
 * @createTime 2022年03月13日 13:48:00
 */
@Data
@Builder
public class UserVo {
    private String phone;
    private String username;
    private String token;
    private String avatar;
    private Boolean isAdmin;
}
