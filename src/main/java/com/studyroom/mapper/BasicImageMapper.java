package com.studyroom.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.studyroom.pojo.BasicImage;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.springframework.stereotype.Repository;

/**
 * @author wuhongbin
 * @createTime 2022年03月13日 11:57:00
 */
@Repository
public interface BasicImageMapper extends BaseMapper<BasicImage> {


    @Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
    @Insert("insert  into t_image (file_name,width,height,is_gif,bucket) values (#{fileName},#{width},#{height},#{isGif},#{bucket})")
    int insertReturnId(BasicImage basicImage);
}
