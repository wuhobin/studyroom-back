package com.studyroom.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.studyroom.pojo.Food;
import com.studyroom.pojo.View;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author wuhongbin
 * @createTime 2022年03月13日 11:59:00
 */
@Repository
public interface ViewMapper extends BaseMapper<View> {

    @Select(" SELECT * FROM t_view WHERE is_delete = 0 ORDER BY id DESC LIMIT #{offset}, #{limit} ")
    List<View> getViewList(@Param("offset") Integer offset, @Param("limit") Integer limit);
}
