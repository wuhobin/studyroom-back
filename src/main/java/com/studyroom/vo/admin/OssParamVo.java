package com.studyroom.vo.admin;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

@Data
public class OssParamVo {
    private MultipartFile officeFile;
    private MultipartFile file;
}
