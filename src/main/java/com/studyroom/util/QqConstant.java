package com.studyroom.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.net.URLEncoder;
import java.util.UUID;

/**
 * @author wuhongbin
 * @version 1.0.0
 * @ClassName QqConstant.java
 * @Description qq常量类
 * @createTime 2021年10月31日 16:16:00
 */
@Component
public class QqConstant {

    @Value("${qq.oauth.redirect_URI}")
    private  String backUrl;
    @Value("${qq.appid}")
    private  String APPID;
    @Value("${qq.appkey}")
    private  String APPKEY;

    /**
     * @return 获取qq登录请求地址
     */
    public  String qqLogin(){
        //用于第三方应用防止CSRF攻击
        String uuid = UUID.randomUUID().toString().replaceAll("-","");
        return "https://graph.qq.com/oauth2.0/authorize?response_type=code"+
                "&client_id=" + APPID +
                "&redirect_uri=" + URLEncoder.encode(backUrl) +
                "&state=" + uuid;
    }

    public  String getAccessToken(String code){
        return "https://graph.qq.com/oauth2.0/token?grant_type=authorization_code"+
                "&client_id=" + APPID +
                "&client_secret=" + APPKEY +
                "&code=" + code +
                "&redirect_uri=" + backUrl;

    }

    public  String getOpenID(String token){
        return "https://graph.qq.com/oauth2.0/me?access_token=" + token;
    }

    public String getUserInfo(String openid,String token){
        return "https://graph.qq.com/user/get_user_info?access_token=" + token +
                "&oauth_consumer_key="+APPID +
                "&openid=" + openid;
    }
}
