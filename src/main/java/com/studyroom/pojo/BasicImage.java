package com.studyroom.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * @author wuhongbin
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "t_image")
public class BasicImage {
    @TableId(type = IdType.AUTO)
    private Integer id;
    private String fileName;
    private Integer width;
    private Integer height;
    private Integer isGif;
    private Integer bucket;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;
}
