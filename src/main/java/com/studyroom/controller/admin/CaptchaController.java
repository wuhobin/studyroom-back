package com.studyroom.controller.admin;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.studyroom.annotation.PassToken;
import com.studyroom.constant.RedisConstantsKey;
import com.studyroom.util.CommonResponseEnum;
import com.studyroom.util.ResponseEntity;
import com.studyroom.util.ResponseUtils;
import com.studyroom.util.SecurityUtils;

import com.zhenzi.sms.ZhenziSmsClient;
import io.lettuce.core.api.sync.RedisCommands;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * @author wuhongbin
 * @version 1.0.0
 * @ClassName CaptchaController.java
 * @Description 发送手机短信验证码
 * @createTime 2021年11月01日 10:26:00
 */
@RestController
public class CaptchaController {

    private final RedisCommands<String,String> redisCommands;

    @Autowired
    public CaptchaController(@Qualifier("syncCommand") RedisCommands<String,String> redisCommands){
        this.redisCommands = redisCommands;
    }

    @Value("${zhenzi.apiUrl}")
    private String apiUrl;

    @Value("${zhenzi.apiId}")
    private String apiId;

    @Value("${zhenzi.appSecret}")
    private String appSecret;

    @Value("${zhenzi.templateId}")
    private String templateId;


    @PassToken
    @PostMapping("/send/captcha")
    public ResponseEntity<?> sendMessage(String phone, HttpServletRequest request) {
        String code;
        try {
            ZhenziSmsClient client = new ZhenziSmsClient(apiUrl, apiId, appSecret);
            code = String.valueOf(new Random().nextInt(999999));
            Map<String, Object> params = new HashMap<>(2);
            params.put("number", phone);
            params.put("templateId", templateId);
            String[] templateParams = new String[2];
            templateParams[0] = code;
            templateParams[1] = "5分钟";
            params.put("templateParams", templateParams);
            String result = client.send(params);
            JSONObject jsonObject = JSON.parseObject(result);
            if (jsonObject.getIntValue("code") == 0){
                String key = String.format(RedisConstantsKey.SMS_CODE_STRING, phone);
                redisCommands.set(key, code);
            }
            return ResponseUtils.success(CommonResponseEnum.SUCCESS, jsonObject.getString("data"));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @PassToken
    @GetMapping("/city/description")
    public ResponseEntity<?> getDescription(){
        String description = redisCommands.get(RedisConstantsKey.CITY_DESCRIPTION);
        return ResponseUtils.success(StringUtils.isEmpty(description) ? "暂无简介" : description);
    }

    @PostMapping("/set/description")
    public ResponseEntity<?> getDescription(String description){
        redisCommands.set(RedisConstantsKey.CITY_DESCRIPTION,description);
        return ResponseUtils.success(null);
    }


}
